package br.ucsal.bes20182.testequalidade.restaurante.business;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20182.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBO {

	public static void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = MesaDao.obterPorNumero(numeroMesa);
		if (SituacaoMesaEnum.LIVRE.equals(mesa.getSituacao())) {
			Comanda comanda = new Comanda(mesa);
			ComandaDao.incluir(comanda);
		} else {
			throw new MesaOcupadaException(numeroMesa);
		}
	}

	public static void incluirItemComanda(Integer codigoComanda, Integer codigoItem, Integer qtdItem)
			throws RegistroNaoEncontrado, ComandaFechadaException {
		Comanda comanda = ComandaDao.obterPorCodigo(codigoComanda);
		Item item = ItemDao.obterPorCodigo(codigoItem);
		comanda.incluirItem(item, qtdItem);
	}

	public static Double fecharComanda(Integer codigoComanda) throws RegistroNaoEncontrado {
		Comanda comanda = ComandaDao.obterPorCodigo(codigoComanda);
		return comanda.fechar();
	}

}
